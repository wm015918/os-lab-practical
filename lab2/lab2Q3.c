
#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>


int main(){
  int child1;
  int child2;

  child1 = fork();
  if(child1 != 0){
    child2 = fork();
  }

  for(int i= 1; i < 10; i++){
    if(child1 == 0){
      printf("Child1 %d process: counter=%d\n", getpid(), i);
      sleep(1);
    }
    else{
      if(child2 == 0){
        printf("Child2 %d process: counter=%d\n", getpid(), i);
        sleep(1);
      }
    else {
    printf("Parent %d process: counter=%d\n", getpid(), i);
    sleep(1);
  }
}
}
return 0;
}
