#include <stdlib.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>


int main(){
  int child1;
  int child2;


  for(int i= 1; i < 11; i++){
    child1 = fork();
    if(child1 == 0){
      printf("Child1 %d process: counter=%d\n", getpid(), i);
      exit(0);
    }
    else{
      child2 = fork();
      if(child2 == 0){
        printf("Child2 %d process: counter=%d\n", getpid(), i);
        exit(0);
      }
      else{
        printf("Parent %d process: counter=%d\n", getpid(), i);
        sleep(2);
      }
    }
}

}
